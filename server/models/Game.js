const { Deck } = require('./Deck');
const { PlayerAction } = require('./Player');

const SMALL_BLIND = 5;
const BIG_BLIND = 10;

class Game {
	constructor (players, onBroadcastState) {
		this.deck = new Deck();
		this.players = players;
		this.playerData = new Map(players.map(player => ([
			player,
			{
				cardsOnHand: [],
				raise: 0,
				hasFolded: false,
				hasPlayedInThisRound: false,
			},
		])));
		this.currentPlayerIndex = 2 % players.length;
		this.cardsOnTable = [];
		this.bank = 0;
		this.onBroadcastState = onBroadcastState;
	}

	getMaxRaise () {
		return Math.max(...[...this.playerData.values()].map(playerData => playerData.raise));
	}

	async playRoundPlayer () {
		const currentPlayer = this.players[this.currentPlayerIndex];
		const playerData = this.getPlayerData(currentPlayer);

		if (playerData.hasFolded) {
			return;
		}

		const raised = this.getMaxRaise() - playerData.raise;

		const currentPlayerAction = await currentPlayer.getAction(raised);

		console.log(currentPlayerAction);

		switch (currentPlayerAction.action) {
			case PlayerAction.FOLD:
				playerData.hasFolded = true;
				break;

			case PlayerAction.RAISE:
				playerData.raise += currentPlayer.betMoney(currentPlayerAction.raise);
				break;
		}
	}

	isPlayerFinishedInThisRound (player) {
		if (player.getMoney() === 0) {
			return true;
		}

		const playerData = this.getPlayerData(player);

		return playerData.hasFolded;
	}

	isRoundFinished () {
		const maxRaise = this.getMaxRaise();

		for (const [player, playerData] of this.playerData.entries()) {
			if (this.isPlayerFinishedInThisRound(player)) {
				continue;
			}

			if (!playerData.hasPlayedInThisRound) {
				return false;
			}

			if (playerData.raise !== maxRaise) {
				return false;
			}
		}

		return true;
	}

	isGameOver () {
		return [...this.playerData.values()].reduce((acc, playerData) => {
			if (!playerData.hasFolded) {
				return acc + 1;
			}

			return acc;
		}, 0) < 2;
	}

	async broadcastState () {
		await this.onBroadcastState();
	}

	async ensureNumberOfCardsOnTable (numberOfCards) {
		while (this.cardsOnTable.length < numberOfCards) {
			this.cardsOnTable.push(this.deck.getRandom());

			await this.broadcastState();
		}
	}

	async playRound () {
		for (const playerData of this.playerData.values()) {
			playerData.hasPlayedInThisRound = false;
		}

		while (!this.isRoundFinished()) {
			const currentPlayer = this.players[this.currentPlayerIndex];

			if (!this.isPlayerFinishedInThisRound(currentPlayer)) {
				await this.playRoundPlayer();
			}

			this.currentPlayerIndex = (this.currentPlayerIndex + 1) % this.players.length;
			this.getPlayerData(currentPlayer).hasPlayedInThisRound = true;

			await this.broadcastState();
		}

		for (const playerData of this.playerData.values()) {
			this.bank += playerData.raise;
			playerData.raise = 0;
		}
	}

	async giveBankToLastPlayer () {
		for (const [player, playerData] of this.playerData.entries()) {
			if (playerData.hasFolded) {
				continue;
			}

			player.addMoney(this.bank);
			this.bank = 0;

			await this.broadcastState();

			return;
		}
	}

	async play () {
		const smallBlindPlayer = this.players[0];
		const bigBlindPlayer = this.players[1];

		this.getPlayerData(smallBlindPlayer).raise += smallBlindPlayer.betMoney(SMALL_BLIND);
		this.getPlayerData(bigBlindPlayer).raise += bigBlindPlayer.betMoney(BIG_BLIND);

		await this.broadcastState();

		for (const playerData of this.playerData.values()) {
			playerData.cardsOnHand.push(this.deck.getRandom(), this.deck.getRandom());
			await this.broadcastState();
		}

		await this.playRound();

		if (this.isGameOver()) {
			await this.giveBankToLastPlayer();
			return;
		}

		await this.ensureNumberOfCardsOnTable(3);
		await this.playRound();

		if (this.isGameOver()) {
			await this.giveBankToLastPlayer();
			return;
		}

		await this.ensureNumberOfCardsOnTable(4);
		await this.playRound();

		if (this.isGameOver()) {
			await this.giveBankToLastPlayer();
			return;
		}
		await this.ensureNumberOfCardsOnTable(5);
		await this.playRound();

		if (this.isGameOver()) {
			await this.giveBankToLastPlayer();
			return;
		}

		const winner = this.evaluateWinner();
		winner.addMoney(this.bank);
		this.bank = 0;

		await this.broadcastState();
	}

	getPlayerData (player) {
		return this.playerData.get(player);
	}

	getActivePlayer () {
		return this.players[this.currentPlayerIndex];
	}

	getCardsOnTable () {
		return this.cardsOnTable;
	}

	getBank () {
		return this.bank;
	}
}

module.exports = {
	Game,
};
