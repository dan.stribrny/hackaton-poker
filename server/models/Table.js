const { Game } = require('./Game');

const TABLE_SIZE = 10;

class Table {
	constructor () {
		this.players = [];
		this.currentGame = null;
		this.nextPlayer = null;
		this.places = new Array(TABLE_SIZE).fill(null);

		this.broadcastState = this.broadcastState.bind(this);
	}

	addPlayer (player) {
		if (!this.nextPlayer) {
			this.nextPlayer = player;
		}

		let position = null;
		for (let i in this.places) {
			if (this.places.hasOwnProperty(i) && !this.places[i]) {
				position = i;
				this.places[i] = player;
				break;
			}
		}

		if (position === null) {
			return false;
		}

		this.players.push(player);

		this.broadcastState().catch(Boolean);

		return true;
	}

	removePlayer (player) {
		if (!this.players.includes(player)) {
			return;
		}

		const nextPlayerIndex = (this.players.indexOf(player) + 1) % this.players.length;

		this.players.splice(this.players.indexOf(player), 1);
		this.places[this.places.indexOf(player)] = null;

		if (this.nextPlayer === player) {
			this.nextPlayer = this.players[nextPlayerIndex] || null;
		}

		this.broadcastState().catch(Boolean);
	}

	async playGame () {
		const nextPlayerIndex = this.players.indexOf(this.nextPlayer);
		const players = this.players.slice(nextPlayerIndex).concat(this.players.slice(0, nextPlayerIndex));

		this.currentGame = new Game(players, this.broadcastState);
		await this.currentGame.play();
		this.currentGame = null;
	}

	async play () {
		while (true) {
			if (this.players.length < 2) {
				console.log('Awaiting players');
			}

			while (this.players.length < 2) {
				await new Promise((resolve) => setTimeout(resolve, 5000));
			}

			console.log('Starting game');
			await this.playGame();

			console.log('Waiting for new round');
			await new Promise((resolve) => setTimeout(resolve, 5000));
		}
	}

	async broadcastState () {
		const activePlayer = this.currentGame && this.currentGame.getActivePlayer();

		const places = this.places.map((player) => {
			if (!player) {
				return null;
			}

			const playerData = this.currentGame && this.currentGame.getPlayerData(player);

			if (!playerData) {
				return {
					pub: {
						id: player.getId(),
						name: player.getName(),
						money: player.getMoney(),
						numberOfCardsOnHand: 0,
						hasFolded: false,
						isActive: false,
						raise: 0,
						cardsOnHand: [],
					},
				};
			}

			return {
				pub: {
					id: player.getId(),
					name: player.getName(),
					money: player.getMoney(),
					numberOfCardsOnHand: playerData.cardsOnHand.length,
					hasFolded: playerData.hasFolded,
					isActive: player === activePlayer,
					raise: playerData.raise,
				},
				priv: {
					cardsOnHand: playerData.cardsOnHand,
				},
			};
		});

		const commonState = {
			cardsOnTable: this.currentGame && this.currentGame.getCardsOnTable() || [],
			bank: this.currentGame && this.currentGame.getBank() || 0,
		};

		const promises = this.places.map((player, index) => {
			if (!player) {
				return;
			}

			const data = {
				...commonState,
				places: places.map((place, placeIndex) => {
					if (!place) {
						return null;
					}

					if (index === placeIndex) {
						return {
							...place.pub,
							...place.priv,
						};
					} else {
						return place.pub;
					}
				}),
			};

			return player.sendState(data);
		});

		await Promise.all(promises);
	}
}

module.exports = {
	Table,
};
