const { Card, CardColor } = require('./Card');

class Deck {
	constructor () {
		let cards = [];
		for (const color of Object.values(CardColor)) {
			for (let rank = 1; rank < 14; rank++) {
				cards.push(new Card(color, rank));
			}
		}

		this.cards = [];
		while (cards.length) {
			const index = Math.floor(Math.random() * cards.length);
			const [card] = cards.splice(index, 1);
			this.cards.push(card);
		}
	}

	/**
	 * @return {Card}
	 */
	getRandom () {
		return this.cards.pop();
	}
}

module.exports = {
	Deck,
};
