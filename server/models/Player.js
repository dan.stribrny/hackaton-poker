const AUTO_FOLD_TIMEOUT_MS = 60000;

const PlayerAction = {
	FOLD: 'FOLD',
	CHECK: 'CHECK',
	RAISE: 'RAISE',
};

class Player {
	constructor (socket, id) {
		this.socket = socket;
		this.id = id;
		this.money = 1000;
		this.name = null;
	}

	getId () {
		return this.id;
	}

	getMoney () {
		return this.money;
	}

	getName () {
		return this.name;
	}

	setName (name) {
		this.name = name;
	}

	betMoney (amount) {
		const moneyTaken = Math.min(this.money, amount);
		this.money -= moneyTaken;
		return moneyTaken;
	}

	addMoney (amount) {
		this.money += amount;
	}

	subscribe (eventName, handler) {
		this.socket.addListener(eventName, handler);
	}

	subscribeOnce (eventName, handler) {
		const h = (...args) => {
			this.unsubscribe(eventName, h);
			handler(...args);
		};

		this.subscribe(eventName, h);
	}

	unsubscribe (eventName, handler) {
		this.socket.removeListener(eventName, handler);
	}

	async getAction (minRaise) {
		this.socket.emit('decide', { minRaise });

		let timeout = null;
		const autoFoldPromise = new Promise((resolve) => {
			timeout = setTimeout(() => {
				timeout = null;
				resolve({ action: PlayerAction.FOLD });
			}, AUTO_FOLD_TIMEOUT_MS);
		});

		const responsePromise = new Promise((resolve) => {
			this.subscribeOnce('decision', (action) => {
				if (minRaise && action.action !== PlayerAction.FOLD &&
					(action.action === PlayerAction.CHECK || (action.action === PlayerAction.RAISE && action.raise < minRaise))) {
					action = { action: PlayerAction.FOLD };
				}

				resolve(action);
			});
		});

		const response = await Promise.race([autoFoldPromise, responsePromise]);

		if (timeout) {
			clearTimeout(timeout);
		}

		return response;
	}

	async sendState (state) {
		this.socket.emit('state', state);
	}

	sendJoinResponse (res) {
		this.socket.emit('join_res', res);
	}
}

module.exports = {
	Player,
	PlayerAction,
};
