const CardColor = {
	DIAMONDS: 'DIAMONDS',
	CLUBS: 'CLUBS',
	HEARTS: 'HEARTS',
	SPADES: 'SPADES',
};

class Card {
	constructor (color, rank) {
		this.color = color;
		this.rank = rank;
	}

	getColor () {
		return this.color;
	}

	getRank () {
		return this.rank;
	}
}

module.exports = {
	CardColor,
	Card,
};
