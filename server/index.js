const { Table } = require('./models/Table');
const { Player } = require('./models/Player');
const app = require('http').createServer(handler);
const io = require('socket.io')(app, { origins: '*:*'});

const table = new Table();

table.play().catch(error => {
	console.log(error);
	process.exit(1);
});

app.listen(8081);

function handler (req, res) {
	res.writeHead(404);
	res.end();
}

let nextPlayerId = 1;

io.on('connection', (socket) => {
	const playerId = nextPlayerId++;
	const player = new Player(socket, playerId);

	player.subscribeOnce('join', ({ name }) => {
		player.setName(name);

		if (!table.addPlayer(player)) {
			player.sendJoinResponse({ error: 'table_full' });
		} else {
			player.sendJoinResponse({ id: playerId });
			table.broadcastState().catch(Boolean);
		}
	});

	player.subscribeOnce('disconnect', () => {
		table.removePlayer(player);
	});
});
