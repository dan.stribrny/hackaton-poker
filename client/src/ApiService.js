import io from 'socket.io-client';

export default class ApiService {
	constructor (socket) {
		this.socket = socket;
	}

	subscribe (eventName, handler) {
		this.socket.on(eventName, handler);
	}

	subscribeOnce (eventName, handler) {
		const h = (...args) => {
			this.unsubscribe(eventName, h);
			handler(...args);
		};

		this.subscribe(eventName, h);
	}

	unsubscribe (eventName, handler) {
		this.socket.off(eventName, handler);
	}

	async join ({ name }) {
		return new Promise((resolve, reject) => {
			this.subscribeOnce('join_res', (res) => {
				if (res.error) {
					reject(new Error(res.error));
				} else {
					resolve(res);
				}
			});

			this.socket.emit('join', { name });
		});
	}

	decision ({ action, raise }) {
		this.socket.emit('decision', { action, raise });
	}

	static async connect (endpoint) {
		return new Promise((resolve, reject) => {
			const socket = io.connect(endpoint);

			socket.on('connect', () => resolve(new ApiService(socket)));
			socket.on('error', reject);
		});
	}

	disconnect () {
		this.socket.disconnect();
	}
}
