import React from 'react';
import P from 'prop-types';

export default class PlayerActions extends React.PureComponent {
	static propTypes = {
		canCheck: P.bool,
		onCheck: P.func,
		onFold: P.func,
		onRaise: P.func,
	};

	state = {
		raise: 0,
	};

	render () {
		return (
			<div className={`th-player-actions`}>
				<div>
					<input onChange={(event) => this.setState({ raise: parseInt(event.currentTarget.value) }) } />
					<button onClick={() => this.props.onRaise(this.state.raise)}>
						Raise
					</button>
				</div>
				<div>
					<button onClick={this.props.onCheck}>
						Check
					</button>
					<button>
						Fold
					</button>
				</div>
			</div>
		)
	}
}
