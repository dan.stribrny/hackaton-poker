import React from 'react';
import P from 'prop-types';
import cx from 'classnames';

export default class Player extends React.PureComponent {
	static protTypes = {
		name: P.string,
		money: P.number,
		isActive: P.bool,
		position: P.number,
		raise: P.number,
	};

	render () {
		return (
			<div className={cx(`th-player th-position-${this.props.position}`, { 'th-player__active': this.props.isActive })}>
				{this.props.name}<br />
				raise: {this.props.raise}<br />
				money: {this.props.money}<br />
				has folded: {this.props.hasFolded ? 'yes' : 'no'}
			</div>
		)
	}
}
