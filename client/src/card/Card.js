import React from 'react';
import P from 'prop-types';

const WIDTH = 73;
const HEIGHT = 98;

export default class Card extends React.PureComponent {
	static protTypes = {
		color: P.string,
		rank: P.number,
	};

	render () {
		const x = this.props.rank === 14 ? 0 : this.props.rank * WIDTH;
		const y = this._getY(this.props.color);

		return (
			<div className={'th-card'} style={ { backgroundPosition: `-${x}px -${y}px` } }>
			</div>
		)
	}

	_getY (color) {
		switch (color) {
			case 'SPADES':
				return HEIGHT;
			case 'HEARTS':
				return HEIGHT * 2;
			case 'DIAMONDS':
				return HEIGHT * 3;
			default:
				return 0;
		}
	}
};
