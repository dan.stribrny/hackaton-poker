import React from 'react';
import P from 'prop-types';
import Card from '../card/Card'

export default class Table extends React.PureComponent {
	static protTypes = {
		cardsOnTable: P.arrayOf(P.shape({
			color: P.string,
			rank: P.number,
		})),
	};

	static defaultProps = {
		cardsOnTable: [],
	};

	render () {
		return (
			<div className="th-table">
				{this.props.children}

				<div className="th-table-cards">
					{this._renderCards()}
				</div>
			</div>
		)
	}

	_renderCards () {
		return this.props.cardsOnTable.map(({ rank, color }, index) => {
			return (
				<Card
					className={`th-table-card-${index}`}
					rank={rank}
					color={color}
				/>
			)
		})
	}
}
