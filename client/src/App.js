import React, { Component } from 'react';
import './App.css';
import Player from './player/Player';
import EmptyPlace from './empty-place/EmptyPlace';
import Table from './table/Table';
import ApiService from './ApiService';
import PlayerActions from './actions/PlayerActions';
import Card from './card/Card';

class App extends Component {
	constructor (...args) {
		super(...args);

		this.state = {
			playerId: null,
			players: new Array(10).fill(null),
			name: '',
			endpoint: 'http://localhost:8081',
			isConnecting: false,
			isConnected: false,
			joinError: null,
		};

		this.service = null;
	}

	render () {
		if (this.state.isConnected) {
			return this._renderTable();
		} else {
			return this._renderJoin();
		}
	}

	_renderJoin () {
		return (
			<form onSubmit={this._handleJoinSubmit}>
				{this.state.joinError && <div className="message-error">{this.state.joinError}</div>}
				<label>Server address: <input value={this.state.endpoint} disabled={this.state.isConnecting}
											  onChange={this._handleEndpointChange}/></label>
				<label>Name: <input value={this.state.name} disabled={this.state.isConnecting}
									onChange={this._handleNameChange}/></label>
				<input disabled={this.state.isConnecting} type="submit" value="Join game"/>
			</form>
		);
	}

	_handleEndpointChange = (event) => {
		this.setState({ endpoint: event.currentTarget.value });
	};

	_handleNameChange = (event) => {
		this.setState({ name: event.currentTarget.value });
	};

	_handleJoinSubmit = async (event) => {
		event.preventDefault();

		if (this.state.isConnecting) {
			return;
		}

		this.setState({ isConnecting: true });

		let id;
		try {
			this.service = await ApiService.connect(this.state.endpoint);

			({ id } = await this.service.join({ name: this.state.name }));
		} catch (error) {
			if (this.service) {
				this.service.disconnect();
				this.service = null;
			}
			this.setState({ joinError: error.message, isConnecting: false });
			return;
		}

		this.service.subscribe('state', this._handleStateChange);

		this.service.subscribe('disconnect', this._handleDisconnect);

		this.setState({ isConnecting: false, isConnected: true, playerId: id });
	};

	_handleDisconnect = () => {
		this.setState({ isConnected: false, playerId: null }, () => {
			this.service = null;
		});
	};

	_handleStateChange = ({ cardsOnTable, bank, places }) => {
		this.setState({
			players: places,
			cardsOnTable,
			bank,
		});
	};

	_renderTable () {
		const currentPlayer = this._getCurrentPlayer();

		console.log(currentPlayer);

		return (
			<div className="App">
				<div className="th-body">
					<Table
						cardsOnTable={this.state.cardsOnTable}
					>
						{this._renderPlayers()}
					</Table>
				</div>

				{currentPlayer && currentPlayer.isActive && <PlayerActions
					onCheck={() => this.service.decision({ action: 'CHECK' })}
					onRaise={(raise) => this.service.decision({ action: 'RAISE', raise })}
				/>}

				<div className="th-player-cards">
					{currentPlayer && currentPlayer.cardsOnHand.map(card => {
						return <Card {...card} />;
					})}
				</div>
			</div>
		);
	}

	_getCurrentPlayer () {
		return this.state.players.find(player => player && player.id === this.state.playerId);
	}

	_renderPlayers () {
		return this.state.players.map((player, index) => {
			if (player) {
				return (
					<Player
						position={index}
						name={player.name}
						money={player.money}
						isActive={player.isActive}
						raise={player.raise}
					/>
				);
			}

			return (
				<EmptyPlace
					position={index}
				/>
			);
		});
	}
}

export default App;
