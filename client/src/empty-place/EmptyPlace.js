import React from 'react';
import P from 'prop-types';


export default class EmptyPlace extends React.PureComponent {
	static protTypes = {
		position: P.number,
	};

	render () {
		return (
			<div className={`th-empty-place th-position-${this.props.position}`}>
			</div>
		)
	}
}
